from django.urls import re_path, path
from . import consumers

websocket_urlpatterns = [
    re_path(r'ws/werewolves/(?P<lobby_name>\w+)/enter', consumers.inGameConsumer.as_asgi()),
    #re_path(r'ws/in_game_lobby/(?P<lobby_name>\w+)/$', consumers.GameConsumer.as_asgi()),
    re_path(r'ws/in_game_lobby/(?P<lobby_name>\w+)/(?P<lobby_status>\w+)/$', consumers.GameConsumer.as_asgi()),
    path("ws/in_game_lobby/", consumers.preLobbyConsumer.as_asgi())
]