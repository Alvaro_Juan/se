
$("#theGame").hide()
        lobby_id = sessionStorage.getItem("lobby_id")
        lobby_id = window.btoa(lobby_id)
        user_role = ""
        user_second_role = ""
        user_name = sessionStorage.getItem("user_name")
        user_type = sessionStorage.getItem("usertype")
        assign_btn = document.getElementById("assign-btn")
        reset_btn = document.getElementById("reset-btn")
        if(user_type == "host"){
            assign_btn.style.display = "block"
            }

        const gameSocket = new WebSocket(  //create a websocket
                'ws://' +
                window.location.host +
                '/ws/werewolves/'+
                lobby_id +
                "/enter/"
        );

        gameSocket.onmessage = function (e){
            data = JSON.parse(e.data)
            switch (data.msgType){
                case "load_players":
                    players_array = data.msg
                    show_players()
                    break
                case "reload_players":
                    players_array = data.msg
                    get_my_role(players_array)
                    show_players()
                    break
                case "assign_roles":
                    players_array = data.msg
                    get_my_role(players_array)
                    show_players()
                    break
                case "page_update":
                    current_turn = data.msg
                    console.log("now it's the turn of:" +current_turn)
                    if ((user_role == data.msg || data.msg == "all")){
                        $("#title").text("Your turn!")
                        if (user_isDead == "true")
                            $("#title").text("You're dead")
                        $('#theGame').fadeIn(1000);
                        new_url = remove_dir_from_url(window.location.href, 2)
                        new_url += "/"+data.msg//?theplayers="+JSON.stringify(players_array)
                        $("#theGame").load(new_url)// {"players":players_array})

                        //$.ajax({url: new_url, success: function(result){
                        //console.log(result);
                        //}});
                        //requireAction = setTimeout(require_action, 5000)

                    }
                    else{
                        $('#theGame').fadeOut(1000);
                        $("#title").text("It's "+ data.msg+"'s turn!")
                    }
                    break
                case "vote_update":
                    voted_player = document.getElementById(data.msg)
                    voted_player_span = document.getElementById(data.msg+"span")
                    if (voted_player != null){
                        voted_player.setAttribute("data-votes", data.votes)
                        voted_player_span.innerText = voted_player.getAttribute("data-votes")
                    }
                    break
                case "kill":
                    player_to_kill = document.getElementById(data.msg+"outside")
                    if (player_to_kill != null /*&& current_turn == "all"*/){
                        player_to_kill.style.textDecoration = "line-through"
                    }
                    break
                case "reset":
                    window.location.reload()
                    players_array = data.msg
                    show_players()
                    window.location.reload()
            }
        }
        $(document).on('click', '#verify', function() {
            clearTimeout(requireAction)
        });
        $(document).on("dblclick", function (){
            clearTimeout(page_update)
        })
        assign_btn.onclick = function (e){
            gameSocket.send(JSON.stringify({
                "msgType" : "assign_roles",
                "msg": "assign_roles",
                "user_name": user_name,
                "user_type": user_type
            }))
            send_page_update()
        }
        reset_btn.onclick = function (){
            gameSocket.send(JSON.stringify({
                "msgType" : "reset",
                "msg": "reset",
                "user_name": user_name,
                "user_type": user_type
            }))
        }
        function require_action(){
            alert("Please select a player!")

        }
        function send_page_update(){
            gameSocket.send(JSON.stringify({
                "msgType":"page_update",
                "msg": "",
                "user_name": user_name,
                "user_type": user_type
            }))
            page_update = setTimeout(send_page_update, 11000);

        }

         function get_my_role(roles_list) {
            for (player in roles_list){
                if (roles_list[player]["user_name"] == user_name){
                    user_role = roles_list[player]["role"]
                    user_second_role = roles_list[player]["second_role"]
                    user_isDead = roles_list[player]["isDead"]
                    console.log("your role is: "+ roles_list[player]["role"])
                }
            }
         }

        function show_players(){
            this_user = sessionStorage.getItem("user_name")
            players_list = document.getElementById("players")
            player_counter = document.getElementById("counter")
            player_counter.innerText = "Number of Players: "+ players_array.length
            players_list.innerHTML = ""
            for (user in players_array) {
                new_player_li = document.createElement("li")
                new_player_li.setAttribute("id",players_array[user]["user_name"]+"outside")
                new_player_li.setAttribute("class","w3-padding-tiny")
                textNode = players_array[user]["user_name"]
                current_player = players_array[user]
                if (current_player["user_name"] == user_name){new_player_li.style.color="green"}
                if(current_player["isDead"] == "true"){new_player_li.style.textDecoration = "line-through"}
                new_player_li.appendChild(document.createTextNode(textNode))
                players_list.appendChild(new_player_li)
                if ((user_role=="werewolf" || user_role=="whitewerewolf") && (current_player["role"]=="werewolf" || current_player["role"]=="whitewerewolf"))
                {
                    fellow_player = document.getElementById(current_player["user_name"]+"outside")
                    var img = document.createElement("img")
                    img.src = "{% static 'images/rsz_rsz_werewolf-small.png' %}"
                    fellow_player.appendChild(img)
                }
                if (user_second_role =="lover" && current_player["second_role"] == "lover" && current_player["user_name"] != user_name){
                    fellow_player = document.getElementById(current_player["user_name"]+"outside")
                    vote_count_span = document.createElement("span")
                    vote_count_span.setAttribute("class", "w3-badge w3-large w3-red")
                    vote_count_span.appendChild(document.createTextNode("\u2764"))
                    fellow_player.appendChild(vote_count_span)
                }

            }
        }

        function remove_dir_from_url(url, numof_dir_to_remove){
            var lastItem = url.split("/")
            for ( var i = 0; i <= numof_dir_to_remove; i++ ) {
                lastItem.pop();
            }
            new_url = lastItem.join("/")
            return new_url
        }
