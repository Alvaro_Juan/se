import json
import os
import discord
import asyncio
import queue
from threading import Thread
lobbyFile = "lobbies.json"


token = "Nzg2NTczMzQzOTI2Nzc5OTE1.X9IXgw.4qrQ2AHSF363JeboRq5Hs6MOIEg"
createQueue = queue.Queue()
deleteQueue = queue.Queue()
botLobbies = []
members = [] # members on discord server

intents = discord.Intents.default()
intents.members = True
client = discord.Client(intents=intents)
guild = None
category = "Game Lobbies"

@client.event
async def on_ready():
    print(f"Logged in as {client.user.name}")
    client.loop.create_task(statusTask(client))
    await initializeGuild(client)
    client.loop.create_task(checkQueues())
    await checkLobbies()

@client.event
async def on_message(message):
    if message.author.bot:
        return
    else:
        if message.content == "testchannel":
            await createTemporaryVC("testchannel", category)
            await asyncio.sleep(3)
            await deleteTemporaryVC("testchannel")

async def statusTask(_client):
    messages = ["Buffing werewolves", "Improving", "Changing rules"]
    while True:
        for message in messages:
            await _client.change_presence(activity=discord.Game(message), status=discord.Status.online)
            await asyncio.sleep(3)

async def initializeGuild(_client):
    global guild
    for guild_ in client.guilds:
        if guild_.name == 'Werewolves':
            guild = guild_
            return True
    return False

async def createTemporaryVC(name, categoryName=category):
    _category = getCategoryByName(categoryName)
    channel = await guild.create_voice_channel(name, category=_category)
    await channel.edit(position=10)
    return channel

async def addPersonToVC(name, VC):
    print(members)
    for member in members:
        if member.display_name == name and member.voice:
            await member.move_to(VC)

async def deleteTemporaryVC(name):
    for channel in guild.voice_channels:
        if channel.name == name:
            await channel.delete()

def getCategoryByName(name):
    for _category in guild.categories:
        if _category.name == name:
            return _category

async def checkQueues():
    # checks if there are new lobbies or deleted lobbies
    global createQueue, members
    while True:
        # used asyncio.sleep() because the discord api doesnt let you create more than 5 channels in a short time
        await asyncio.sleep(1)
        while not createQueue.empty():
            c_lobby = createQueue.get()
            channel = await createTemporaryVC(c_lobby["room_name"])
            members = guild.members
            print(c_lobby)
            for person in c_lobby["users"]:
                await addPersonToVC(person["user_name"], channel)
        while not deleteQueue.empty():
            await deleteTemporaryVC(deleteQueue.get())

async def checkLobbies(jsonFile=lobbyFile):
    global botLobbies
    with open(jsonFile) as lobbies:
        all_lobbies = json.load(lobbies)
        if all_lobbies != botLobbies:
            added = [i for i in all_lobbies if i not in botLobbies]
            for lobby in added:
                await addLobby(lobby)
            deleted = [i for i in botLobbies if i not in all_lobbies]
            for lobby in deleted:
                await deleteLobby(lobby)


async def addLobby(lobby):
    global createQueue
    createQueue.put(lobby)

async def deleteLobby(lobby):
    await deleteTemporaryVC(lobby["room_name"])


def startBot(_createQ, _deleteQ):
    global createQueue, deleteQueue
    createQueue = _createQ
    deleteQueue = _deleteQ
    client.run(token)


def main():
    global lobbyFile
    createVoiceChannelQueue = queue.Queue()
    deleteVoiceChannelQueue = queue.Queue()
    lobbyFile = "../lobbies.json"
    t = Thread(target=startBot, args=(createVoiceChannelQueue, deleteVoiceChannelQueue,))
    t.start()
    # for j in range(10):
    #
    #     createVoiceChannelQueue.put(f"lobby{j}")
    #     deleteVoiceChannelQueue.put(f"lobby{j}")


if __name__ == "__main__":
    main()
    pass