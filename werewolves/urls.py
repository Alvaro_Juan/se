from django.urls import path, re_path

from . import views

app_name = "werewolves"

urlpatterns = [
    path("", views.redirect_to_lobby, name="default"),

    path("in_game_lobby/", views.base, name="in_game"),

    path('in_game_lobby/<str:lobby_name>/<str:lobby_status>/', views.lobby, name="lobby"),

    path("<str:lobby_name>/enter/", views.index, name="index"),

    path("all", views.day, name="day"),

    #path("day", views.day, name="day"),

    path("setup", views.setup, name="setup"),

    path("setup_night", views.setup_night, name="setup_night"),

    path("setup_night_werewolves", views.setup_night_werewolves, name="setup_night_werewolves"),

    path("setup_night_white_werewolf", views.setup_night_white_werewolf, name="setup_night_white_werewolf"),

    path("setup_night_seer", views.setup_night_seer, name="setup_night_seer"),

    path("setup_night_witch", views.setup_night_witch, name="setup_night_witch"),

    path("hunter", views.hunter, name="hunter"),

    #path("setup_night_hunter", views.setup_night_hunter, name="setup_night_hunter"),

    #path("setup_night_amor", views.setup_night_amor, name="setup_night_amor"),
    path("amor", views.setup_night_amor, name="setup_night_amor"),

    path("night", views.night, name="night"),

    path("villagers_win", views.villagers_win, name="villagers_win"),

    path("werewolves_win", views.werewolves_win, name="werewolves_win"),

    path("lovers_win", views.lovers_win, name = "lovers_win"),

    path("whitewerewolf_win", views.whitewerewolf_win, name = "whitewerewolf_win"),

    #path("end_game", views.end_game, name="end_game"),

    path("seer", views.seer_skill, name="seer_skill"),

    #path("seer_skill", views.seer_skill, name="seer_skill"),

    path("werewolf", views.werewolves_skill, name="werewolves_skill"),#(?P<theplayers>\w{0,50})/$

    path("villager", views.sleep_walking, name="sleep_walking"),

    path("whitewerewolf", views.white_skill, name="white_skill"),

    #path("white_skill", views.white_skill, name="white_skill"),

    path("witch", views.witch_skill, name="witch_skill"),

    ##path("witch_skill", views.witch_skill, name="witch_skill"),

    path("sleep_1", views.sleep_1, name="sleep_1"),

    path("sleep_accident", views.sleep_accident, name="sleep_accident"),

]
