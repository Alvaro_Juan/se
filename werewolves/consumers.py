# here is where a websocket connection will be handled
# django-channels was used here in order to make a Asynchronous connection possible

## how it works
# this class has to have three methods:
# connect - to accept the connection
# disconnect - to disconnect
# receive - to receive messages from the websocket

# django_channels works by treating each new connection as channels
# and then when a new channel has come than that channel will either create a group or join one
# in which other channels(users) can later enter(if the group_name is provided).

# methods
# self.channel_layer.group_add(group_name, self.channel_name--this one is always like this)
# adds the incoming channel(user) to the specified group_name if it exists if not it creates a nw group with that name

# self.channel_layer.group_send(group_name, {
# "type": theMethodThatWillHandleTheMessageSending
# "custom variables here": value
# see the examples
#           })

# self.channel_layer.group_discard(group_name, self.channel_name--always like this)
# remove the channel(user) with the name self.channel_name from the group with the name 'group_name'
import asyncio
import base64
import inspect
import random

from . import views
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
import json
asyncio.set_event_loop(asyncio.new_event_loop()) # need a event loop to run discord bot
times = 0

# Discord Bot
from werewolves.DiscordBot import *

createVoiceChannelQueue = queue.Queue()
deleteVoiceChannelQueue = queue.Queue()
_lobbyFile = "/lobbies.json"
t = Thread(target=startBot, args=(createVoiceChannelQueue, deleteVoiceChannelQueue,))
t.start()


class GameConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user_name = None
        self.user_type = ""
        self.lobby_name = self.scope['url_route']['kwargs']['lobby_name']
        self.lobby_status = self.scope["url_route"]["kwargs"]["lobby_status"]
        self.game_group_name = base64.b64decode(self.lobby_name).decode('utf-8')
        self.channel_layer2 = get_channel_layer()
        self.delete_rooms_from_file_on_disconnect = True

        await self.channel_layer.group_add(
            self.game_group_name,
            self.channel_name,
        )
        await self.accept()
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                "type": "noOfConnections",
                "msg": len(self.channel_layer2.groups[self.game_group_name]),
            }
        )

        await self.store_rooms()

    async def receive(self, text_data):
        stack = inspect.stack()
        the_class = stack[1][0].f_locals["self"].__class__.__name__
        the_method = stack[1][0].f_code.co_name
        #print("I was called by {}.{}()".format(the_class, the_method))
        text_data = json.loads(text_data)
        message = text_data["message"]
        self.user_name = text_data["username"]
        self.user_type = text_data["usertype"]
        msg_type = text_data["msgType"]
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                "type": "communicate",
                "msgType": msg_type,
                "message": message,
                "user_name": self.user_name,
                "usertype": self.user_type,
            }
        )

    async def disconnect(self, text_data, code=None, delete_room=None):
        all_groups = self.channel_layer2.groups
        await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )
        msg = len(all_groups[self.game_group_name]) if self.game_group_name in all_groups else 0
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                "type": "disconnected",
                "msg": msg
            }
        )
        if self.delete_rooms_from_file_on_disconnect:
            await self.store_rooms(delete_user=self.user_name)

    async def communicate(self, event):
        message = event["message"]
        user_name = event["user_name"]
        msg_type = event["msgType"]
        user_type = event["usertype"]
        if message == "delete_room":
            await self.send(text_data=json.dumps({
                "msgType": msg_type,
                "msg": message,
            }))

            if self.game_group_name in self.channel_layer2.groups:
                del self.channel_layer2.groups[self.game_group_name]
            await self.store_rooms()


        elif message == "game_start":
            self.delete_rooms_from_file_on_disconnect = False
            await self.store_rooms(change_available=True)
            await self.send(text_data=json.dumps({
                "msgType": msg_type,
                "msg": event["message"],
            }))

        elif message == "add_new_user":
            await self.store_rooms(add_user=user_name)
            await self.send(text_data=json.dumps({
                "msgType": msg_type,
                "msg": event["message"],
            }))

        elif message == "delete_user":
            await self.store_rooms(delete_user = user_name)
            await self.send(text_data=json.dumps({
                "msgType": msg_type,
                "msg": event["message"],
            }))

        await self.send(text_data=json.dumps({
            "msgType": msg_type,
            "msg": event["message"],
            "user_name": event["user_name"]
        }
        ))

    async def noOfConnections(self, event):
        counter = event["msg"]
        await self.send(text_data=json.dumps(
            {
                "msgType": "countConnections",
                "msg": counter
            }
        ))

    async def disconnected(self, event):
        msg = event["msg"]
        await self.send(text_data=json.dumps({
            "msgType": "countConnections",
            "msg": msg
        }))

    async def store_rooms(self, change_available=None, add_user=None, delete_user=None):  # stores all groups and their channels in a json file
        rooms = []
        rooms_to_send = []
        with open("lobbies.json") as reading:
            data_inside = json.load(reading)
            for key in range(0, len(data_inside)):
                if data_inside[key]["room_name"] in self.channel_layer2.groups.keys() and data_inside[key]["room_name"] not in rooms:
                    if add_user is not None and data_inside[key]["room_name"] == self.game_group_name and not any(d.get('user_name') == add_user for d in data_inside[key]["users"]):
                        data_inside[key]["users"].append({"user_name":add_user, "role":"none", "isDead":"false", "second_role":"none"})
                    elif delete_user is not None and data_inside[key]["room_name"] == self.game_group_name and any(d.get('user_name') == delete_user for d in data_inside[key]["users"]):
                        data_inside[key]["users"] = list(filter(lambda i: i["user_name"] != delete_user, data_inside[key]["users"]))
                    rooms_to_send.append(data_inside[key])
                    rooms.append(data_inside[key]["room_name"])

        with open("lobbies.json", "w", newline="") as writing:
            if self.game_group_name in self.channel_layer2.groups.keys() and change_available == None and add_user is None and delete_user is None:
                user = f"{self.user_name}({self.user_type})"
                data = {"room_name": self.game_group_name, "isAvailable": "true","users":[],
                        "lobby_status": self.lobby_status}
                if self.user_name: data.update({"users": [{"user_name":self.user_name, "role":"none", "isDead":"false", "second_role":"none"}]})
                rooms_to_send.append(data)
            elif change_available is not None:
                for key in rooms_to_send:
                    if key["room_name"] == self.game_group_name:
                        key["isAvailable"] = str(not change_available).lower()
            json.dump(rooms_to_send, writing, ensure_ascii=False, indent=4)
        await views.send_lobby_update_to_pre_lobby()






class preLobbyConsumer(AsyncWebsocketConsumer):  # the socket which will be used by pre_lobby(base.html)
    async def connect(self):
        self.group_name = "preLobby2"
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )

        await self.accept()
        with open('lobbies.json') as f:
            data = json.load(f)

        await self.channel_layer.group_send(
            self.group_name,
            {
                "type": "send_list_of_lobbies",
                "msgType": "regular",
                "messages": data
            }
        )
        await self.load_lobbies()

    async def receive(self, text_data):
        pass

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    async def send_list_of_lobbies(self, event):
        msg = event["messages"]
        msgtype = event["msgType"]
        await self.send(text_data=json.dumps({
            "msgType": msgtype,
            "msg": msg

        }))

    async def load_lobbies(self):
        stack = inspect.stack()
        the_class = stack[1][0].f_locals["self"].__class__.__name__
        the_method = stack[1][0].f_code.co_name

        print("I was called by {}.{}()".format(the_class, the_method))
        with open('lobbies.json') as f:
            data = json.load(f)



theroles = ["amor", "seer", "villager","werewolf", "witch", "all", "whitewerewolf"]
turn = -1
villagers = []
villagers_only = []
werewolves = []
seer = {"isDead":"false", "user_name":""}
witch = {"isDead":"false", "user_name":"", "hasKillPotion":"true", "hasHealPotion":"true"}
whitewerewolf = {"isDead":"false", "user_name":""}
hunter = {"isDead":"false", "user_name":""}
amor = {"isDead":"false", "user_name":""}
last_killed_players = []
dead_second_lover = [] # this will hold the name of the lover that was killed as a consequence of its lover being killed
last_killed_players_roles = []
lovers = []
votes = []
sleepwalker_votes = []
alive_players = []

class inGameConsumer(AsyncWebsocketConsumer):  # the socket which will take care of the game session

    async def connect(self):
        global theroles
        self.user_name = ""
        self.user_type = ""
        self.lobby_name = self.scope['url_route']['kwargs']['lobby_name']
        self.game_group_name = base64.b64decode(self.lobby_name).decode('utf-8')
        self.channel_layer2 = get_channel_layer()
        self.all_players = self.get_players()
        self.check_if_group_exists() # if this group doesn't exist then set all the above global variables to default

        await self.channel_layer.group_add(
            self.game_group_name,
            self.channel_name
        )
        await self.accept()
        players = self.get_players()
        await self.channel_layer.group_send(
            self.game_group_name,
            {
              "type": "load_players",
              "msgType": "load_players",
              "msg": players,
            }
        )


    async def receive(self, text_data):
        global turn
        global last_killed_players
        global last_killed_players_roles
        global alive_players
        global witch
        global sleepwalker_votes
        text_data = json.loads(text_data)
        msgType = text_data["msgType"]
        message = text_data["msg"]
        user_name = text_data["user_name"]
        self.user_name = user_name
        user_type = text_data["user_type"]

        if msgType == "assign_roles":
            global theroles
            theroles = ["amor", "seer", "villager", "werewolf", "witch", "all", "whitewerewolf"]
            get_roles = await self.assign_roles()
            #if user_type == "host":
                #self.start_timer()
            await self.channel_layer.group_send(
                self.game_group_name,
                {
                    "type": "communicate",
                    "msgType": msgType,
                    "msg": get_roles,
                    "votes":"",
                    "user_name": user_name,
                    "user_type": user_type
                }
            )
        elif msgType == "page_update":
            role = theroles[turn]
            turn += 1
            if turn >= len(theroles):turn = 1
            if user_type == "host":
                await self.channel_layer.group_send(
                    self.game_group_name,
                    {
                        "type": "turn_tracker",
                        "msgType": msgType,
                        "page_to_show": role,
                        "role": role,
                        "turn": turn,
                        "user_type": user_type,
                    }
                )

        elif msgType == "vote_update":
            global votes
            thevotes = int(text_data["votes"])
            thevotes += 1
            votes.append({"user_name":message, "votes": thevotes})
            await self.channel_layer.group_send(
                self.game_group_name,
                {
                    "type": "communicate",
                    "msgType": msgType,
                    "msg": message,
                    "votes": str(thevotes),
                    "user_name": user_name,
                    "user_type": user_type
                }
            )
        elif msgType == "kill":
            if message == "": # if the messageType kill was sent from the werewolves page, then the message will be empty because in that case we need to count the votes then to kill the player
                maximum = 0
                player = None
                for p in votes:
                    if p["votes"] > maximum: maximum = p["votes"];player = p["user_name"]
                votes = []
                #if "sleepwalking" in theroles: theroles.remove("sleepwalking");
                if player is not None:
                    await self.check_dead_player(player)
            else: # whereas in the case of the whitewerewolf we don't need to count any votes to find whom should we kill because the whitewerewolf page has already sent the player to kill
                player_killed = message
                if player_killed is not None:
                    await self.check_dead_player(player_killed)

        elif msgType == "immediate_kill":
            maximum = 0
            player = None
            for p in votes:
                if p["votes"] > maximum: maximum = p["votes"];player = p["user_name"]
            votes = []
            last_killed_players_roles = []
            last_killed_players = []
            if player is not None:
                await self.check_dead_player(player, kill_immediately="Hello")

        elif msgType == "lovers_selected":
            global lovers
            get_lovers = message
            await self.change_attribute_of_player(get_lovers[0], second_role="lover")
            await self.change_attribute_of_player(get_lovers[1], second_role="lover")
            lovers.extend(get_lovers)
            await self.channel_layer.group_send(self.game_group_name,{
                "type": "send_lovers",
                "msgType": "lovers_selected",
                "player1": get_lovers[0],
                "player2": get_lovers[1],
            })

        elif msgType == "witch_heal":
             healed_player_id = message
             await self.check_healed_player(healed_player_id)


        elif msgType == "witch_kill":
           player_killed_by_witch = message
           witch["hasKillPotion"] = "false"
           if player_killed_by_witch is not None or player_killed_by_witch != "":
               await self.check_dead_player(player_killed_by_witch)

        elif msgType == "hunter_kill":
            turn -=1
            #print(last_killed_players_roles)
            last_killed_players_roles.remove('hunter')
            player_killed = message
            if player_killed is not None or player_killed != "":
                await self.check_dead_player(player_killed)


        elif msgType == "sleepwalk_yes_answer":
            villager_voted_yes = message
            sleepwalker_votes.append(villager_voted_yes)

        elif msgType == "sleepwalk_finished":
             await self.check_sleepwalking_votes()

        elif msgType == "lucky_villager_finished":
            turn -=1
            theroles.remove("sleepwalking")
            sleepwalker_votes = []

        elif msgType == "reset":
            await self.reset_game()


    async def check_healed_player(self, healed_player_id):
        global werewolves
        global whitewerewolf
        global seer
        global villagers
        global villagers_only
        global lovers
        global amor
        global witch
        global hunter
        global lovers
        global dead_players
        global last_killed_players
        global alive_players


        player_to_be_healed = healed_player_id
        get_player_healed_role = await self.change_attribute_of_player(player_to_be_healed, dead="false")

        if get_player_healed_role[0] == "werewolf" and player_to_be_healed in werewolves:
            werewolves.append(player_to_be_healed)
        elif get_player_healed_role[0] == "villager" and player_to_be_healed in villagers:
            villagers.append(player_to_be_healed)
            villagers_only.append(player_to_be_healed)
        elif get_player_healed_role[0] == "whitewerewolf":
            whitewerewolf = {"isDead": "false", "user_name": player_to_be_healed}
        elif get_player_healed_role[0] == "witch" and player_to_be_healed in villagers:
            witch["isDead"] = "false"
            villagers.append(player_to_be_healed)
        elif get_player_healed_role[0] == "seer" and player_to_be_healed in villagers:
            seer = {"isDead": "false", "user_name": player_to_be_healed}
            villagers.append(player_to_be_healed)
        elif get_player_healed_role[0] == "amor" and player_to_be_healed in villagers:
            amor = {"isDead": "false", "user_name": player_to_be_healed}
            villagers.append(player_to_be_healed)
        elif get_player_healed_role[0] == "hunter" and player_to_be_healed in villagers:
            hunter = {"isDead": "false", "user_name": player_to_be_healed}
            villagers.append(player_to_be_healed)
        if get_player_healed_role[1] == "lover" and "lover" in last_killed_players_roles:
            for lover in lovers:
                if lover != player_to_be_healed:
                    await self.change_attribute_of_player(lover, dead="false")

                    alive_players.append(lover)
                    last_killed_players_roles.remove(get_player_healed_role[1])
                    dead_second_lover.remove(lover)
                    lovers = [player_to_be_healed, lover]
                    lovers_send = lovers
                    await self.check_healed_player(lover)
                    break

        if player_to_be_healed not in alive_players: alive_players.append(player_to_be_healed)
        if player_to_be_healed in last_killed_players: last_killed_players.remove(player_to_be_healed)
        last_killed_players_roles.remove(get_player_healed_role[0])
        witch["hasHealPotion"] = "false"

    async def check_sleepwalking_votes(self):
        global votes
        global sleepwalker_votes
        global villagers_only
        global theroles
        global turn
        if len(sleepwalker_votes) == 1:
            lucky_villager = sleepwalker_votes[0]
            #print("sending lucky")
            theroles.insert(3, "sleepwalking")
            #print(turn)
            #print(theroles[turn])
            #await self.channel_layer.group_send(
               # self.game_group_name,
                #{
                 #   "type": "give_seer_ability_to_villager",
                  #  "msgType": "page_update",
                   # "page_to_show": "seer",
                   # "role":"lucky_villager",
                   # "lucky_villager":lucky_villager
               # }
           # )
        elif len(sleepwalker_votes) == len(villagers_only):
            random_victim = random.choice(villagers_only)
            await self.check_dead_player(random_victim)
            sleepwalker_votes = []
        else:
            # do nothing
            sleepwalker_votes = []

    async def give_seer_ability_to_villager(self, event):
        await self.send(text_data=json.dumps({
            "msgType": event["msgType"],
            "page_to_show": event["page_to_show"],
            "role": event["role"],
            "lucky_villager": event["lucky_villager"]
        }))

    async def turn_tracker(self, event):
        global turn
        global theroles
        global witch
        global amor
        global seer
        global hunter
        global werewolves
        global alive_players
        global last_killed_players
        global dead_second_lover
        global last_killed_players_roles
        global villagers
        global votes
        global sleepwalker_votes
        msgType = event["msgType"]
        usertype = event["user_type"]
        role = theroles[turn]
        json_to_send = {}
        if role == "amor":
            if amor["isDead"] == "false":
                json_to_send = {
                    "msgType": "page_update",
                    "page_to_show": "amor",
                    "role":"amor",
                    "players": self.all_players,
                    "phase":"dayy",
                }
            else:turn += 1; role = theroles[turn]


        elif role == "seer":
            if seer["isDead"] == "false":
                json_to_send = {
                    "msgType": "page_update",
                    "page_to_show": "seer",
                    "role":"seer",
                    "players": self.all_players,
                    "phase": "nightt",
                }
            else: turn += 1;role = theroles[turn]


        elif role == "villager": # for the sleepwalking
            if len(villagers_only) > 0:
                json_to_send = {
                    "msgType": "page_update",
                    "page_to_show": "villager",
                    "role": "villager",
                    "players": villagers,
                    "phase": "nightt",
                }
            else:turn += 1;role = theroles[turn]

        elif role == "sleepwalking":
            json_to_send={
                "msgType": "page_update",
                "page_to_show": "seer",
                "role": "sleepwalk",
                "lucky_villager": sleepwalker_votes[0],
                "phase": "nightt",
            }
        elif role == "werewolf":
            if len(werewolves) > 0:
                    json_to_send = {
                    "msgType": "page_update",
                    "page_to_show": "werewolf",
                    "role": "werewolf",
                    "players": werewolves,
                    "phase": "nightt",
                }
            else:
                turn += 1; role = theroles[turn]

        elif role == "witch":
            print(f"witch is {last_killed_players_roles}")
            if witch["isDead"] == "false" or "witch" in last_killed_players_roles:
                json_to_send = {
                    "msgType": "page_update",
                    "page_to_show" : "witch",
                    "role": "witch",
                    "alive_players": alive_players,
                    "last_killed_players": last_killed_players,
                    "hasKillPotion": witch["hasKillPotion"],
                    "hasHealPotion": witch["hasHealPotion"],
                    "phase": "nightt",
                }
            else: turn += 1; role = theroles[turn]


        if role == "all": # day
            await self.prepare_the_day_voting()



        elif role == "whitewerewolf":
            print(f"whitewerewolf is {whitewerewolf['isDead']}")
            if whitewerewolf["isDead"] == "false":
                json_to_send = {
                    "msgType": "page_update",
                    "page_to_show": "whitewerewolf",
                    "role": "whitewerewolf",
                    "werewolves": werewolves,
                    "phase": "nightt",
                }
            else:turn = 1; await self.turn_tracker(event)


        await self.send(text_data=json.dumps(
            json_to_send
        ))
        return



    async def prepare_the_day_voting(self):
        global last_killed_players
        global alive_players
        global villagers
        global lovers
        global werewolves
        global whitewerewolf
        global last_killed_players_roles
        global turn
        global votes
        global theroles


        if "hunter" in last_killed_players_roles:
            await self.send(text_data=json.dumps({

                "msgType": "page_update",
                "page_to_show": "hunter",
                "role": "hunter",
                "players": alive_players,
                "phase": "nightt",

            }))
            return

        else:
            lovers_killed = False
            lovers_send = []
            global lovers


            if "lover" in last_killed_players_roles:
                lovers_killed = True
                lovers_send = lovers

            json_to_send = {"msgType": "page_update",
                        "role": "all",
                        "players":last_killed_players,
                        "lovers_killed": lovers_send}
            print(f"werewolves left {werewolves}\n villagers left {villagers}")
            #check if lovers have won
            if len(alive_players) == 2 and len(lovers) == 2:
                json_to_send.update({"page_to_show":"lovers_win"})


            if len(villagers) == 0 and whitewerewolf["isDead"] == "true": # check if werewolves have won
                json_to_send.update({"page_to_show": "werewolves_win"})

            elif len(werewolves) == 0 and len(villagers) == 0 and whitewerewolf["isDead"] == "false": # check if the whitewerewolf has won
                json_to_send.update({"page_to_show": "whitewerewolf_win"})


            elif len(werewolves) == 0 and whitewerewolf["isDead"] == "true": # check if the villagers have won
                json_to_send.update({"page_to_show": "villagers_win"})

            else: # if no one won then go ahead and anounce the dead players and start the voting
                players = self.get_players()
                await self.send(text_data=json.dumps({ #first anounce the deaths
                    "msgType": "announce_deaths",
                    "dead_players": last_killed_players,
                    "players": players,
                    "lovers_killed": lovers_send,
                    "phase": "dayy",
                }))
                json_to_send.update({"page_to_show": "all"}) # then go ahead and show the voting stage
            await self.send(text_data=json.dumps(json_to_send))



    async def disconnect(self, code):
       await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )

    async def reload_players(self, votes, username, usertype):
        players = self.get_players()
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                "type": "communicate",
                "msgType": "reload_players",
                "msg": players,
                "votes": votes,
                "user_name": username,
                "user_type": usertype
            }
        )

    async def send_lovers(self, event):
        msgType = event["msgType"]
        player1 = event["player1"]
        player2 = event["player2"]
        await self.send(text_data=json.dumps({
            "msgType": msgType,
            "player1": player1,
            "player2": player2,
        }))
        await self.reload_players("", "", "")

    async def communicate(self, event):
        msgType = event["msgType"]
        message = event["msg"]
        user_name = event["user_name"]
        user_type = event["user_type"]
        await self.send(text_data=json.dumps({
                "msgType": msgType,
                "msg": message,
                "votes": event["votes"],
                "user_name": event["user_name"]
        }))


    def get_players(self):
        with open("lobbies.json") as lobbies:
            all_lobbies = json.load(lobbies)
            for key in range(0, len(all_lobbies)):
                if all_lobbies[key]["room_name"] == self.game_group_name:
                    self.all_players = all_lobbies[key]["users"]
                    return all_lobbies[key]["users"]



    async def assign_roles(self):
        roles = ["villager", "seer", "werewolf", "witch", "amor", "whitewerewolf", "hunter"]
        possible_roles = roles
        possible_players = [player["user_name"] for player in self.all_players]
        assigned_roles = []
        while len(possible_players) != 0:
            if not all(items in assigned_roles for items in roles):
                player_chosen = random.choice(possible_players)
                possible_players.remove(player_chosen)
                role_chosen = possible_roles.pop()
                await self.change_attribute_of_player(player_chosen, role=role_chosen)
                assigned_roles.append(role_chosen)
                await self.fill_array(role_chosen, player_chosen)
            else:
                possible_roles.extend(["werewolf", "villager"])
                role_chosen = random.choice(possible_roles)
                player_chosen = random.choice(possible_players)
                possible_players.remove(player_chosen)
                await self.change_attribute_of_player(player_chosen, role=role_chosen)
                await self.fill_array(role_chosen, player_chosen)
        return self.all_players

    async def fill_array(self,role_chosen,player_chosen):
        global villagers
        global werewolves
        global seer
        global witch
        global whitewerewolf
        global alive_players
        global villagers_only

        global hunter
        if role_chosen == "werewolf":
            werewolves.append(player_chosen)
            alive_players.append(player_chosen)
        if role_chosen == "witch":
            witch = {"isDead":"false", "user_name":player_chosen, "hasKillPotion":"true", "hasHealPotion":"true"} # isDead, player_name, hasHealPotion, hasKillPotion
            villagers.append(player_chosen)
            alive_players.append(player_chosen)
        if role_chosen == "seer":
            seer = {"isDead":"false", "user_name":player_chosen}
            villagers.append(player_chosen)
            alive_players.append(player_chosen)
        if role_chosen == "whitewerewolf":
            whitewerewolf = {"isDead":"false", "user_name":player_chosen}
            alive_players.append(player_chosen)
        if role_chosen == "hunter":
            hunter = {"isDead":"false", "user_name":player_chosen}
            villagers.append(player_chosen)
            alive_players.append(player_chosen)
        if role_chosen == "amor":
            amor = {"isDead":"false", "user_name":player_chosen}
            villagers.append(player_chosen)
            alive_players.append(player_chosen)
        if role_chosen == "villager":
            villagers.append(player_chosen)
            villagers_only.append(player_chosen)
            alive_players.append(player_chosen)


    async def update_lobbies(self):
        with open("lobbies.json") as reading:
            data_inside = json.load(reading)
            for lobbies in range(0, len(data_inside)):
                if data_inside[lobbies]["room_name"] == self.game_group_name:
                    data_inside[lobbies]["users"] = self.all_players
                    break

        with open("lobbies.json", "w", newline="") as writing:
            json.dump(data_inside, writing, ensure_ascii=False, indent=4)


    async def change_attribute_of_player(self, player_name, role=None, dead=None, second_role=None):
        player_role = ""
        player_second_role = ""
        with open("lobbies.json") as reading:
            data_inside = json.load(reading)
            for key in range(0, len(data_inside)):
                for user in data_inside[key]["users"]:
                    if user["user_name"] == player_name:
                        if role is not None: user["role"] = role
                        if dead is not None: user["isDead"] = dead
                        if second_role is not None: user["second_role"] = second_role
                        player_role = user["role"]
                        player_second_role = user["second_role"]
                        break
        with open("lobbies.json", "w") as writing:
            json.dump(data_inside, writing, ensure_ascii=False, indent=4)
        self.get_players()
        return player_role, player_second_role



    async def load_players(self, text_data):
        msgType = text_data["msgType"]
        message = text_data["msg"]
        await self.send(text_data=json.dumps({
            "msgType":msgType,
            "msg":message
        }))


    def send_message(self):
        print("hello")
        global theroles
        global turn
        role = theroles[turn]
        turn += 1
        if turn >= len(theroles): turn = 1
        self.channel_layer.group_send(
            self.game_group_name,
            {
                "type": "turn_tracker",
                "msgType": "page_update",
                "page_to_show": role,
                "role": role,
                "turn": turn,
                "user_type": "",
            }
        )

    async def check_if_group_exists(self):
        if self.game_group_name not in self.channel_layer2.groups.keys():
             await self.reset_game_attributes()

        else:
            pass




    async def reset_game_attributes(self):
        global theroles
        global turn
        global votes
        global sleepwalker_votes
        global villagers
        global villagers_only
        global werewolves
        global seer
        global witch
        global whitewerewolf
        global dead_players
        global last_killed_players
        global dead_second_lover
        global lovers
        global hunter
        global amor
        global alive_players
        global last_killed_players_roles

        theroles = ["amor", "seer", "villager", "werewolf", "witch", "all", "whitewerewolf"]
        turn = -1
        votes = []
        sleepwalker_votes = []
        villagers = []
        villagers_only = []
        werewolves = []
        seer = {"isDead":"false", "user_name":""}
        witch = {"isDead":"false", "user_name":"", "hasKillPotion":"true", "hasHealPotion":"true"}
        whitewerewolf = {"isDead":"false", "user_name":""}
        amor = {"isDead":"false", "user_name":""}
        hunter = {"isDead":"false", "user_name":""}
        dead_players = []
        last_killed_players = []
        dead_second_lover = []
        last_killed_players_roles = []
        lovers = []
        alive_players = []

    async def reset_game(self):
        await self.reset_game_attributes()
        with open("lobbies.json") as reading:
            data_inside = json.load(reading)
            for key in range(0,len(data_inside)):
                for user in data_inside[key]["users"]:
                    user["role"] = "none"
                    user["isDead"] = "false"
                    user["second_role"] = "none"
        with open("lobbies.json", "w") as writing:
            json.dump(data_inside, writing, ensure_ascii=False, indent=4)
        await self.send_reset_message("","","")


    async def send_reset_message(self, username, usertype, votes):
        players = self.get_players()
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                "type": "communicate",
                "msgType": "reset",
                "msg": players,
                "votes": votes,
                "user_name": username,
                "user_type": usertype
            }
        )

    async def check_dead_player(self, player, kill_immediately=False, add_to_last_killed_players=True):
        global werewolves
        global whitewerewolf
        global seer
        global villagers
        global villagers_only
        global lovers
        global amor
        global witch
        global hunter
        global lovers
        global dead_players
        global last_killed_players
        global alive_players

        lovers_send = ""
        player_to_be_killed = player
        get_player_killed_role = await self.change_attribute_of_player(player_to_be_killed, dead="true")

        if get_player_killed_role[0] == "werewolf" and player_to_be_killed in werewolves:
            werewolves.remove(player_to_be_killed)
        elif get_player_killed_role[0] == "villager" and player_to_be_killed in villagers:
            villagers.remove(player_to_be_killed)
            villagers_only.remove(player_to_be_killed)
        elif get_player_killed_role[0] == "whitewerewolf":
            whitewerewolf = {"isDead":"true", "user_name":player_to_be_killed}
        elif get_player_killed_role[0] == "witch" and player_to_be_killed in villagers:
            witch["isDead"] = "true"
            villagers.remove(player_to_be_killed)
        elif get_player_killed_role[0] == "seer" and player_to_be_killed in villagers:
            seer = {"isDead":"true", "user_name":player_to_be_killed}
            villagers.remove(player_to_be_killed)
        elif get_player_killed_role[0] == "amor" and player_to_be_killed in villagers:
            amor = {"isDead":"true", "user_name":player_to_be_killed}
            villagers.remove(player_to_be_killed)
        elif get_player_killed_role[0] == "hunter" and player_to_be_killed in villagers:
            hunter = {"isDead":"true", "user_name":player_to_be_killed}
            villagers.remove(player_to_be_killed)
        if get_player_killed_role[1] == "lover" and "lover" not in last_killed_players_roles:
            for lover in lovers:
                if lover != player_to_be_killed:
                    second_player_to_be_killed = lover
                    await self.change_attribute_of_player(lover, dead="true")

                    if lover in alive_players:alive_players.remove(lover)
                    last_killed_players_roles.append(get_player_killed_role[1])
                    dead_second_lover.append(lover)
                    lovers = [player_to_be_killed, lover]
                    lovers_send = lovers
                    await self.check_dead_player(lover, add_to_last_killed_players = False)
                    break

        if player_to_be_killed in alive_players:alive_players.remove(player_to_be_killed)
        if add_to_last_killed_players is True:
            last_killed_players.append(player_to_be_killed)
        last_killed_players_roles.append(get_player_killed_role[0])
        print(player_to_be_killed, last_killed_players, last_killed_players_roles)
        if kill_immediately:
            players = self.get_players()
            await self.channel_layer.group_send(
                self.game_group_name,
                {
                    "type": "send_immediate_announcement",
                    "msgType": "announce_deaths",
                    "dead_players": "",
                    "players": players,
                    "lovers_killed": lovers_send
                }
            )


    async def send_immediate_announcement(self, event):
        msgType = event["msgType"]
        dead_players = event["dead_players"]
        players = event["players"]
        lovers_killed = event["lovers_killed"]
        await self.send(text_data=json.dumps({  # first anounce the deaths
            "msgType": "announce_deaths",
            "dead_players": dead_players,
            "players": players,
            "lovers_killed": lovers_killed,
        }))